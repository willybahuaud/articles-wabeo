<?php
/**
 * © Willy Bahuaud (le temps de sortir l'article)
 Pour tester
 */
// add_action( 'init', 'willy_test_q' );
function willy_test_q() {
	$test = new WP_Query( array( 
			'post_type' => 'post', 
			'geo_query' => array( 
				'lat'      => 1.45,
				'lng'      => 1.42, 
				'distance' => 200, // en km
				'comapre'  => '<=' // par défaut
				) 
		) );
}

// add_filter( 'pre_get_posts', 'willy_test_geo' );
function willy_test_geo( $q ) {
	if ( $q->is_main_query() ) {
		$q->set( 'geo_query', array( 
				'lat'      => 1.45, 
				'lng'      => 1.42, 
				'distance' => 200 ) );
	}
	return $q;
}

// add_filter( 'the_posts', 'show_query', 10, 2 );
function show_query( $posts, $q ) {
	var_dump( $q );
}

// add_action( 'init', 'willy_test_users' );
function willy_test_users() {
	$test = new WP_User_Query( array( 
			'geo_query' => array( 
				'lat' => 1.45, 
				'lng' => 1.42, 
				'distance' => 200 ) 
		) );
}

/**
 Les codes
 *
 * Les deux séries de fonctions qui suivent permettent de faire des requêtes géolocalisées avec WordPress, 
 * sur les tables des post ou bien des utilisateurs.
 *
 * Il s'agit d'étendre les possibilités de WP Query, en lui faisant supporter des nouveaux paramètres :
 * -> geo_query
 * --> lat
 * --> lng
 * --> distance
 *
 * Et de pouvoir utiliser `distance`en tant que valeur d'orderby
 *
 * La distance est même directement renvoyé dans l'objet `post`
 */

// POSTS
add_filter( 'posts_join', 'willy_geo_join', 10, 2 );
function willy_geo_join( $join, $q) {
	if ( isset( $q->query_vars['geo_query']['lat'],
		  $q->query_vars['geo_query']['lng'],
		  $q->query_vars['geo_query']['distance'] ) ) {
		global $wpdb;
		$join .= " INNER JOIN $wpdb->postmeta AS metalat ON ( $wpdb->posts.ID = metalat.post_id )";
		$join .= " INNER JOIN $wpdb->postmeta AS metalng ON ( $wpdb->posts.ID = metalng.post_id )";
	}
	return $join;
}

add_filter( 'posts_where', 'willy_geo_where', 10, 2 );
function willy_geo_where( $where, $q ) {
	if ( isset( $q->query_vars['geo_query']['lat'],
		  $q->query_vars['geo_query']['lng'],
		  $q->query_vars['geo_query']['distance'] ) ) {

		global $wpdb;
		$where .= " AND metalat.meta_key = 'lat'";
		$where .= " AND metalng.meta_key = 'lng'";
	}
	return $where;
}

add_filter( 'posts_fields', 'willy_geo_fields', 10, 2 );
function willy_geo_fields( $fields, $q ) {
	if ( isset( $q->query_vars['geo_query']['lat'],
		  $q->query_vars['geo_query']['lng'],
		  $q->query_vars['geo_query']['distance'] ) ) {

		global $wpdb;
		$fields .= $wpdb->prepare(", ( 6371 * acos( cos( radians( %f ) ) 
						* cos( radians( metalat.meta_value ) ) 
						* cos( radians( metalng.meta_value ) - radians( %f ) ) 
						+ sin( radians( %f ) ) 
						* sin( radians( metalat.meta_value ) ) ) ) AS distance",
		              $q->query_vars['geo_query']['lat'],
					  $q->query_vars['geo_query']['lng'],
					  $q->query_vars['geo_query']['lat'] );
	}
	return $fields;
}

add_filter( 'posts_groupby', 'willy_geo_distance', 10, 2 );
function willy_geo_distance( $groupby, $q ) {
	if ( isset( $q->query_vars['geo_query']['lat'],
		  $q->query_vars['geo_query']['lng'],
		  $q->query_vars['geo_query']['distance'] ) ) {

		$compare = '<=';
		if ( isset( $q->query_vars['geo_query']['compare'] )
		  && in_array( $q->query_vars['geo_query']['compare'], array( '<', '<=', '>', '>=' ) ) ) {
		  	$compare = $q->query_vars['geo_query']['compare'];
		}
		global $wpdb;
		$groupby .= $wpdb->prepare( "$wpdb->posts.ID HAVING distance $compare %d",
					  $q->query_vars['geo_query']['distance'] );
	}
	return $groupby;
}

add_filter( 'posts_orderby', 'willy_geo_orderby_distance', 10, 2 );
function willy_geo_orderby_distance( $orderby, $q ) {
	if ( isset( $q->query_vars['orderby'],
		  $q->query_vars['geo_query']['lat'],
		  $q->query_vars['geo_query']['lng'],
		  $q->query_vars['geo_query']['distance'] )
	  && 'distance' == $q->query_vars['orderby'] ) {

		$order = in_array( $q->query_vars['order'], array( 'ASC', 'DESC' ) ) ? $q->query_vars['order'] : 'ASC';
		$orderby = 'distance ' . $order;
	}
	return $orderby;
}

// USERS
add_filter( 'pre_user_query', 'willy_geo_users' );
function willy_geo_users( $q ) {
	if ( isset( $q->query_vars['geo_query']['lat'],
		  $q->query_vars['geo_query']['lng'],
		  $q->query_vars['geo_query']['distance'] ) ) {

		// Opérateur de comparaison
		$compare = '<=';
		if ( isset( $q->query_vars['geo_query']['compare'] )
		  && in_array( $q->query_vars['geo_query']['compare'], array( '<', '<=', '>', '>=' ) ) ) {
		  	$compare = $q->query_vars['geo_query']['compare'];
		}

		global $wpdb;

		// Colonne distance
		$q->query_fields .= $wpdb->prepare(", ( 6371 * acos( cos( radians( %f ) ) 
						* cos( radians( metalat.meta_value ) ) 
						* cos( radians( metalng.meta_value ) - radians( %f ) ) 
						+ sin( radians( %f ) ) 
						* sin( radians( metalat.meta_value ) ) ) ) AS distance",
					  $q->query_vars['geo_query']['lat'],
					  $q->query_vars['geo_query']['lng'],
					  $q->query_vars['geo_query']['lat'] );

		// Jointures
		$q->query_from .= " INNER JOIN $wpdb->usermeta AS metalat ON ( $wpdb->users.ID = metalat.user_id )";
		$q->query_from .= " INNER JOIN $wpdb->usermeta AS metalng ON ( $wpdb->users.ID = metalng.user_id )";

		$q->query_where .= " AND metalat.meta_key = 'lat'";
		$q->query_where .= " AND metalng.meta_key = 'lng'";

		// Order by
		if ( 'distance' == $q->query_vars['orderby'] 
		  && in_array( $q->query_vars['order'], array( 'ASC', 'DESC' ) ) ) {
			$q->query_orderby = 'ORDER BY distance ' . $q->query_vars['order'];
		}

		// Group by et Having
		$q->query_orderby = $wpdb->prepare( "GROUP BY $wpdb->users.ID HAVING distance $compare %d ",
					  $q->query_vars['geo_query']['distance'] ) . $q->query_orderby;

	}
}

// SELECT SQL_CALC_FOUND_ROWS wp_users.*, ( 6371 * acos( cos( radians( 1.45 ) ) 
// * cos( radians( metalat.meta_value ) ) 
// * cos( radians( metalng.meta_value ) - radians( 1.45 ) ) 
// + sin( radians( 1.45 ) ) 
// * sin( radians( metalat.meta_value ) ) ) ) AS distance FROM wp_users INNER JOIN wp_usermeta AS metalat ON ( wp_users.ID = metalat.user_id ) INNER JOIN wp_usermeta AS metalng ON ( wp_users.ID = metalng.user_id ) WHERE 1=1 AND metalat.meta_key = 'lat' AND metalng.meta_key = 'lng' HAVING distance < 200 ORDER BY distance ASC
