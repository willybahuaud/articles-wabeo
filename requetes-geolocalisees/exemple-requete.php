<?php
// Exemple de requête Géolocalisée
$test = new WP_Query( array( 
			'post_type' => 'post', 
			'geo_query' => array( 
				'lat'      => 1.45,
				'lng'      => 1.42, 
				'distance' => 200, // en km
				'compare'  => '<=' // par défaut
				) 
		) );