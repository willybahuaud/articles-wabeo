
SET @lat_user = 48.85; #latitude à comparer
SET @lng_user = 2.35; #longitude à comparer
SET @rayon_terre = 6371; # en km

SELECT wp_posts.*, ( @rayon_terre * acos( cos( radians( @lat_user ) ) 
* cos( radians( metalat.meta_value ) ) 
* cos( radians( metalng.meta_value ) - radians( @lng_user ) ) 
+ sin( radians( @lat_user ) ) 
* sin( radians( metalat.meta_value ) ) ) ) AS distance 
FROM wp_posts

INNER JOIN wp_postmeta AS metalat 
ON ( wp_posts.ID = metalat.post_id ) 

INNER JOIN wp_postmeta AS metalng 
ON ( wp_posts.ID = metalng.post_id ) 

WHERE 1=1 
AND wp_posts.post_type = 'post' #type de contenu
AND metalat.meta_key = 'lat' #clé de la meta latitude
AND metalng.meta_key = 'lng' #clé de la meta longitude