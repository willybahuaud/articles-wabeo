<?php
// Je créer le shortcode
add_shortcode( 'bouton', 'wabeo_bouton' );
function wabeo_bouton( $atts, $content = '', $tag ) {
	// Je filtre les attributs
	// Le dernier paramètre de shortcode_atts…
	// … $tag permet d'utiliser le filtre shortcode_atts_{$tag}
	$atts = shortcode_atts( array(
	    	'ancre' => 'Prendre contact',
	    	'lien'  => 'mailto:' . antispambot( get_option( 'admin_email' ) ),
	        ), $atts, $tag ); 
	// Je retourne le html de substitution
	return '<a href="' . esc_attr( $atts['lien'] ) . '" class="bouton">' 
		. esc_html( $atts['ancre'] ) . '</a>';
}

/**
 * [bouton/]
 * ↳ <a href="mailto:willy@wabeo.fr" class="bouton">Prendre contact</a>
 *
 * [bouton lien="http://pastacode.wabeo.fr" ancre="Découvrez le plugin"]
 * ↳ <a href="http://pastacode.wabeo.fr" class="bouton">Découvrez le plugin</a>
 */
