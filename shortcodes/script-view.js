(function() {

	function set_shortcodes_atts( editor, atts ) {

		// nom fenetre
		var titreFenetre = !_.isUndefined( atts.nom ) ? atts.nom : 'Ajouter un shortcode';
		// balise du shortcode
		var balise = !_.isUndefined( atts.balise ) ? atts.balise : false;

		fn = function() {
			editor.windowManager.open( {
				title: titreFenetre,
				body: atts.body,
				onsubmit: function( e ) {
					var out = '[' + balise;
					for ( var attr in e.data ) {
						out += ' ' + attr + '="' + e.data[ attr ] + '"';
					}
					out += '/]';
					editor.insertContent( out );
				},
			} );
		};
		return fn;
	}

	tinymce.PluginManager.add('monscript', function( editor, url ) {

		editor.addButton('mon_bouton', {
			icon: false,
			text:'mes shortcodes',
			type:'menubutton',
			menu: [
				{
					text: 'bouton',
					onclick: set_shortcodes_atts( editor, {
						body: [
							{
								label: 'Texte du bouton',
								name: 'ancre',
								type: 'textbox',
								value: '',
							},
							{
								label: 'URL du lien',
								name: 'lien',
								type: 'textbox',
								value: '',
							}
						],
						balise: 'bouton',
						nom: 'Ajouter un bouton',
					} ),
				}, 
				{
					text: 'conversion de devises',
					onclick: set_shortcodes_atts( editor, {
						body: [
							{
								label: 'Valeur',
								name: 'valeur',
								type: 'textbox',
								tooltip: 'Saisissez un nombre',
								value: '',
							},
							{
								label: 'De',
								name: 'de',
								type: 'listbox',
								values : [
			                        { text: 'Euros', value: 'EUR' },
			                        { text: 'Livres Sterling', value: 'GBP' },
			                        { text: 'Dollards Américains', value: 'USD' },
			                    ]
							},
							{
								label: 'Vers',
								name: 'vers',
								type: 'listbox',
								values : [
			                        { text: 'Euros', value: 'EUR' },
			                        { text: 'Livres Sterling', value: 'GBP' },
			                        { text: 'Dollards Américains', value: 'USD' },
			                    ]
							}
						],
						balise: 'conversion',
						nom: 'Conversion de devises',
					} ),
				}
			]
		});
	});

	function getAttr( str, name ) {
		name = new RegExp( name + '=\"([^\"]+)\"' ).exec( str );
		return name ? window.decodeURIComponent( name[1] ) : '';
	}

	window.wp.mce.views.register( 'bouton', {
	    initialize: function() {
		    var titre = '<div class="my-view-wrapper">';
		    	titre += '<p><strong>Bouton</strong></p>';
		    var ancre = getAttr( this.text, 'ancre' );
		    if ( ancre ) {
		    	titre += '<p>« ' + _.escape( ancre ) + ' »</p>';
		    }

			titre += '</div>';
		    this.render( titre );
		},
	} );
})();