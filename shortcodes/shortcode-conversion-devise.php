<?php
add_shortcode( 'conversion', 'wabeo_conversion' );
function wabeo_conversion( $atts, $content = '', $tag ) {
    $atts = shortcode_atts( array(
            'valeur' => false,
            'de'     => 'EUR',
            'vers'   => 'GBP',
            ), $atts, $tag ); 
    if ( $atts['valeur'] ) {
        $sens = $atts['de'] . $atts['vers'];
        if ( ! $taux = get_transient( 'taux_' . $sens ) ) {
            $data = wp_remote_get( 'http://finance.yahoo.com/webservice/v1/symbols/' . $sens . '=X/quote?format=json' );
            if ( ! is_wp_error( $data )
              && '200' == wp_remote_retrieve_response_code( $data ) ) {
                $data = json_decode( wp_remote_retrieve_body( $data ) );
                if ( isset( $data->list->resources[0]->resource->fields->name )
                  && $atts['de'] . '/' . $atts['vers'] == $data->list->resources[0]->resource->fields->name ) {
                    $taux = $data->list->resources[0]->resource->fields->price;
                    set_transient( 'taux_' . $sens, floatval( $taux ), DAY_IN_SECONDS );
                }
            }
        }
        if ( $taux ) {
            return ( floatval( $atts['valeur'] ) * $taux );
        }
        return $atts['valeur'];
    }
    return;
}

/**
 * [conversion value="1"/]
 * ↳ 0.715725
 *
 * [conversion from="AUD" to="USD" value="10"/]
 * ↳ 7.79880
 */