(function() {
	tinymce.PluginManager.add('monscript', function( editor, url ) {

		// https://www.tinymce.com/docs/demo/custom-toolbar-menu-button/
		editor.addButton('mon_bouton', {
			icon: false,
			text:'mes shortcodes',
			type:'menubutton',
			menu: [
				{
					text: 'bouton',
					onclick: function() {
						editor.insertContent('[bouton/]');
					}
				}, {
					text: 'conversion de devises',
					onclick: function() {
						editor.insertContent('[conversion valeur="5"/]');
					}
				}
			]
		});
	});

})();