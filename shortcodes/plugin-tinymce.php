<?php
add_action( 'admin_init', 'wabeo_tinymce_button' );
function wabeo_tinymce_button() {
	if ( ! current_user_can('edit_posts') 
	  && ! current_user_can('edit_pages') ) {
        return false;
    }

    if ( get_user_option('rich_editing') == 'true') {
		add_filter( 'mce_external_plugins', 'wabeo_script_tiny' );
		add_filter( 'mce_buttons', 'wabeo_register_button' );
	}
}

function wabeo_register_button( $buttons ) {
	array_push( $buttons, '|', 'mon_bouton' );
	return $buttons;
}

function wabeo_script_tiny( $plugin_array ) {
	$plugin_array['monscript'] = plugins_url( '/script.js', __FILE__ );
	return $plugin_array;
}

add_action( 'admin_init', 'add_wabeo_styles_to_editor' );
function add_wabeo_styles_to_editor() {
	global $editor_styles;
	$editor_styles[] = plugins_url( '/styles.css', __FILE__ );
}