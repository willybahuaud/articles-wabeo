(function() {
	tinymce.PluginManager.add('monscript', function( editor, url ) {

		// https://www.tinymce.com/docs/demo/custom-toolbar-button/
		editor.addButton('mon_bouton', {
			icon: false,
			text:'bouton',
			onclick: function () {
		    	editor.insertContent('[bouton/]');
		    }
		});
	});

})();