<?php
add_shortcode( 'emphase', 'wabeo_emphase' );
function wabeo_emphase( $atts, $content = '', $tag ) {
	$atts = shortcode_atts( array(
	    	'attachment-id' => false,
	        ), $atts, $tag ); 
	if ( $content ) {
		if ( $atts['attachment-id'] 
		  && 'attachment' == get_post_type( $atts['attachment-id'] ) ) {
			if ( $img = wp_get_attachment_image_src( $atts['attachment-id'], 'large' ) ) {
				return '<div class="text-wrapper" style="background-image:url(' . $img[0] . ');"><div>'
					. $content
					. '</div></div>';
			}
		}
		return '<div class="text-wrapper"><div>'
			. $content
			. '</div></div>';
	}
	return;
}

/**
 * [emphase]
 * text
 * [/emphase]
 * ↳ <div class="text-wrapper"><div>text</div></div>
 *
 * [emphase attachment-id="4"]
 * text
 * [/emphase]
 * ↳ <div class="text-wrapper" style="background-image:url(http://exemple.com/wp-content/uploads/image.jpg);">
 * ↳ <div>text</div></div>
 */

?>
<style>
	.text-wrapper{
		padding:2em;
		font-size:1.6em;
		box-sizing:border-box;
	}
	.text-wrapper > div{
		background-color:rgba(255,255,255,0.8);
	}
</style>

