<?php
add_action('template_redirect', 'willy_search_url_rewrite_rule');
function willy_search_url_rewrite_rule() {
    if ( isset( $_GET['s'] ) 
      && ( get_query_var( 'ville' ) 
		|| get_query_var( 'chambres' ) 
		|| get_query_var( 'quartiers' ) 
		|| get_query_var( 'prix-mini' )
		|| get_query_var( 'prix-maxi' )
		|| get_query_var( 'equipements' ) ) ) {

    		$q_args = array();
    		foreach( array( 
    		    'ville',
    		    'quartiers',
    		    'chambres',
				'prix-mini',
				'prix-maxi',
				'equipements' ) as $q_arg ) {
    			if ( $a = get_query_var( $q_arg, false ) ) {
    				$q_args[ $q_arg ] = $a;
    			}
    		}

    		$search_url = add_query_arg( $q_args, $search_url );
			wp_redirect( $search_url );
			exit();
    }
}
 
add_filter( 'search_rewrite_rules', 'willy_search_rewrite_rules' );
function willy_search_rewrite_rules( $search_rewrite ) {
    $search_rewrite['recherche-logements/?$'] = 'index.php?s=';
    return $search_rewrite;
}