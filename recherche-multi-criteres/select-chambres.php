<p>
	<label for="chambres">Nombre de chambres</label>	
	<select name="chambres" id="chambres">
		<option value=""><?php _e( 'Indifférent' ); ?></option>
		<?php 
		// Valeur antiérieure ?
		$nb_chambres = intval( get_query_var( 'chambres' ) );

		// On boucle
		for ( $i = 1; $i <= 10; $i++ ) { 
			echo '<option value="' . $i . '" ' 
			// on utilise selected() pour assigner… 
			// … selected="selected" s'il y a lieu
			. selected( $i, $nb_chambres, false ) 
			. '>' . sprintf( _n( '%d chambre', '%d chambres', $i ), $i ) . '</option>';
		} ?>
	</select>
</p>