<?php
if ( ! empty( $tax_queries ) ) {
	// S'il y a plus de deux tax_query, 
	// on indique que les deux doivent être remplies
	if ( isset( $tax_queries[1] ) ) {
		$tax_queries['relation'] = 'AND';
	}
	// On pousse notre tax_query
	$q->set( 'tax_query', $tax_queries );
}