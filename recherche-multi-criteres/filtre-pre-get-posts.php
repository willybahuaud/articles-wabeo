<?php
add_filter( 'pre_get_posts', 'willy_pre_get_posts' );
function willy_pre_get_posts( $q ) {
	// $q est l'objet de la class WP_Query
	// S'il s'agit de la main query…
	if ( $q->is_main_query() 
	  // … sur une page de recherche…
	  && is_search() 
	  // … multi-critères
	  && ( $q->get( 'ville' ) 
	    || $q->get( 'quartiers' ) 
	    || $q->get( 'prix-mini' ) 
	    || $q->get( 'prix-maxi' ) 
	    || $q->get( 'chambres' ) 
	    || $q->get( 'equipements' ) ) ) {
		
		// Ça nous concerne donc !
		$q->set( 'post_type', 'logement' );
		
	}
	return $q;
}