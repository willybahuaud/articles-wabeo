<?php
// on récupère ou on créer un tableau tax_query
$tax_queries = $q->get( 'tax_query', array() );

// Si des quartiers sont demandés
if ( $q->get( 'quartiers' ) ) {
	$tax_queries[] = array(
		'taxonomy' => 'localisation',
		'terms'    => array_filter( (array) $q->get( 'quartiers' ) ),
		'field'    => 'slug',
	    );
} 
// sinon, si des villes sont demandées
elseif ( $q->get( 'ville' ) ) {
	$tax_queries[] = array(
		'taxonomy' => 'localisation',
		'terms'    => $q->get( 'ville' ),
		'field'    => 'slug',
	    );
}