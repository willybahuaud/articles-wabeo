<?php
add_filter( 'the_title', 'willy_emphase_search_words' );
add_filter( 'the_excerpt', 'willy_emphase_search_words' );
function willy_emphase_search_words( $content ) {
    if ( is_search() 
      && in_the_loop() 
      && is_main_query() ) {
        if ( preg_match_all( '/".*?("|$)|((?<=[\t ",+])|^)[^\t ",+]+/', get_search_query(), $matches ) ) {
            $stopwords = explode( ',', _x( 'about,an,are,as,at,be,by,com,for,from,how,in,is,it,of,on,or,that,the,this,to,was,what,when,where,who,will,with,www',
            'Comma-separated list of search stopwords in your language' ) );
            $terms = array_diff( $matches[0], $stopwords );
            $content = str_ireplace( $terms, array_map( 'willy_add_mark', $terms ), $content );
        }
    }
    return $content;
}

function willy_add_mark( $elem ) {
    return '<mark>' . $elem . '</mark>';
}