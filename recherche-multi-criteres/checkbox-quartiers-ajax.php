<?php
add_action( 'wp_ajax_select_quartiers', 'willy_select_quartiers' );
add_action( 'wp_ajax_nopriv_select_quartiers', 'willy_select_quartiers' );
function willy_select_quartiers() {
	$ville = $_GET['ville'];
	if ( $ville ) {
		$args = array( 'ville' => strval( $ville ) );
		$checklist = wp_quartiers_dropdown( $args );
		wp_send_json_success( array( 'checklist' => $checklist ) );
	} else {
		wp_send_json_error();
	}
}

add_action( 'wp_enqueue_scripts', 'willy_register_select_quartiers' );
function willy_register_select_quartiers() {
	wp_enqueue_script( 'checkbox-quartiers', get_template_directory_uri() . '/js/checkbox-quartiers.js', array( 'jquery' ) );
	wp_enqueue_script( 'checkbox-quartiers' );
	wp_localize_script( 'checkbox-quartiers', 'adminAjax', admin_url( 'admin-ajax.php' ) );
}