<?php
foreach ( array(
	'balcon',
	'garage',
	'jardin',
	'piscine',
    ) as $equipement ) {
	// Pour chaque équipement, s'il se trouve dans la query_var $equipements
	if ( is_array( $q->get( 'equipements' ) ) 
	  && in_array( $equipement, $q->get( 'equipements' ) ) ) {
		// on exclue les biens qui n'ont pas cet équipement
		$meta_queries[] = array(
			'key'     => $equipement,
			'value'   => 'non',
			'compare' => '!=',
		    );
	} 
}