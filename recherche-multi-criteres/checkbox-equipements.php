<?php 
$equips = array(
	'balcon'  => __( 'Balcon' ),
	'garage'  => __( 'Garage' ),
	'jardin'  => __( 'Jardin' ),
	'piscine' => __( 'Piscine' ),
    );
foreach ( $equips as $key => $equip ) {
	echo '<p>';
		echo '<input type="checkbox" name="equipements[]" '
		. 'id="equipment[' . $key . ']" value="' . $key . '"'
		// Si l'équipement doit être pré-coché, d'une précédente recherche…
		// … alors la fonciton checked renverra checked="checked"
		. checked( in_array( $key, get_query_var( 'equipements' ) ), true, false ) . '> ';
		echo '<label for="equipment[' . $key . ']">' . $equip . '</label>';
	echo '</p>';
} ?>