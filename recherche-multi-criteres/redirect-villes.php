<?php
add_action( 'template_redirect', 'willy_redirect_recherche_ville' );
function willy_redirect_recherche_ville() {
	// s'il s'agit d'une recherche avec la ville de choisie
	if ( is_search() && get_query_var( 'ville' ) 
      // mais que les autres champs son vides
      && ! get_query_var( 'chambres' ) 
      && ! get_query_var( 'quartiers' ) 
      && ! get_query_var( 'prix-mini' ) 
      && ! get_query_var( 'prix-maxi' ) 
      && ! get_query_var( 'equipements' ) ) {
		// et que la ville existe
		if ( $ville = term_exists( get_query_var( 'ville' ) , 'localisation' ) ) {
			// alors on redirige l'utilisateur sur la page du terme de taxonomie
			wp_redirect( get_term_link( $ville['term_taxonomy_id'], 'localisation' ), 303 );
			exit();
		}
	}
}