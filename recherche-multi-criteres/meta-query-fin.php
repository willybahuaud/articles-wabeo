<?php
if ( ! empty( $meta_queries ) ) {
	// S'il y a plus de deux meta_query, 
	// on indique que les deux doivent être remplies
	if ( isset( $meta_queries[1] ) ) {
		$meta_queries['relation'] = 'AND';
	}
	// On pousse notre tax_query
	$q->set( 'meta_query', $meta_queries );
}