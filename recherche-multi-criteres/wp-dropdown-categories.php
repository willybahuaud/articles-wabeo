<p>
<label for="ville">Ville</label>
<?php 
	$args = array(
		'show_option_all'   => __( 'Toutes les villes' ), 
		'orderby'           => 'name', 
		'order'             => 'ASC',
		'show_count'        => 1,
		'hide_empty'        => 1, 
		'echo'              => 0,
		'name'              => 'ville', 
		'id'                => 'ville',
		'hierarchical'      => true,
		'depth'             => 1,
		'taxonomy'          => 'localisation',
		'hide_if_empty'     => true, 
		'value_field'       => 'slug',
	);

	// Y-a-t'il une ville actuellement sélectionnée ?
	if ( get_query_var( 'ville' ) 
	    && ( $t = term_exists( get_query_var( 'ville' ), 'localisation' ) ) ) {
		$args['selected'] = get_query_var( 'ville' ) ;
	}

	$list = wp_dropdown_categories( $args );

	// Afficher la liste s'il existe des villes associées à des contenus
	if ( $list ) {
		echo $list;
	} ?>
</p>