<?php
add_filter( 'pre_get_posts', 'willy_push_price' );
function willy_push_price( $q ) {
	$prix = array();
	// prix mini
	if ( $q->get( 'prix-mini' )
	  && ( (int) $q->get( 'prix-mini' ) > 0 ) ) {
		$prix['min'] = intval( $q->get( 'prix-mini' ) );
	}

	// prix maxi
	if ( $q->get( 'prix-maxi' )
	  && ( (int) $q->get( 'prix-maxi' ) > 0 ) ) {
		$prix['max'] = intval( $q->get( 'prix-maxi' ) );
	}

	if ( ! empty( $prix ) ) {
		$q->set( 'prix', $prix );
	}
}

add_filter( 'posts_join', 'willy_prix_join', 10, 2 );
function willy_prix_join( $join, $q ) { // $q est l'instance de WP_Query
	if ( isset( $q->query_vars['prix'] ) ) {
		global $wpdb;
		// Dans les modifications des requêtes SQL… 
		// … il faut toujours faire précéder un ajout d'une expace
		$join .= " INNER JOIN $wpdb->postmeta AS prix ON ( $wpdb->posts.ID = prix.post_id )";
	}
	return $join;
}

add_filter( 'posts_where', 'willy_prix_where', 10, 2 );
function willy_prix_where( $where, $q ) {
	if ( isset( $q->query_vars['prix'] ) ) {
		global $wpdb;
		$where .= " AND prix.meta_key LIKE('lot_%_prix')";
		if ( isset( $q->query_vars['prix']['min'] ) ) {
			$where .= " AND prix.meta_value >= " . intval( $q->query_vars['prix']['min'] );
		}		
		if ( isset( $q->query_vars['prix']['max'] ) ) {
			$where .= " AND prix.meta_value <= " . intval( $q->query_vars['prix']['max'] );
		}
	}
	return $where;
}

add_filter( 'posts_groupby', 'willy_prix_groupeby', 10, 2 );
function willy_prix_groupeby( $groupby, $q ) {
	if ( isset( $q->query_vars['prix'] ) ) {
		global $wpdb;
		$groupby .= "$wpdb->posts.ID";
	}
	return $groupby;
}