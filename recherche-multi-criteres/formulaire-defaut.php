<form action="<?php echo home_url( '/' ); ?>">
	<!-- Ici on affiche le champ « s »
	mais nous aurions pu également en faire 
	un champ de type hidden avec une valeur vide-->
	<p>
		<label for="s">Rechercher</label>
		<input type="text" name="s" value="<?php the_search_query(); ?>" id="s">
	</p>
	<button type="submit">Rechercher</button>
</form>