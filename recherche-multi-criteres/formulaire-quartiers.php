<!-- suite du formulaire… -->
<div id="quartier-wrapper">	
<?php
if ( get_query_var('ville') ) {
	$args = array( 'ville' => get_query_var( 'ville' ) );
	if ( get_query_var( 'quartiers' ) ) {
		$args['current'] = get_query_var( 'quartiers' );
	}
	echo wp_quartiers_dropdown( $args );
} ?>
</div>