<p>
	<label for="prix-mini">Prix minimum</label>
	<input type="number" name="prix-mini" min="0" value="<?php 
	if ( isset( $_GET['prix-mini'] ) && $_GET['prix-mini'] ) {
		echo intval( $_GET['prix-mini'] );
	} ?>" id="prix-mini">
</p>

<p>
	<label for="prix-maxi">Prix maximum</label>
	<input type="number" name="prix-maxi" min="0" value="<?php 
	if ( isset( $_GET['prix-maxi'] ) && $_GET['prix-maxi'] ) {
		echo intval( $_GET['prix-maxi'] );
	} ?>" id="prix-maxi">
</p>