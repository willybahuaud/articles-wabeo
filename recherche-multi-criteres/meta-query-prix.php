<?php
// Si un prix maximum et minimum sont précisés
if ( $q->get( 'prix-mini' ) && $q->get( 'prix-maxi' ) ) {
	$meta_queries[] = array(
		'key'     => 'prix',
		'value'   => array( $q->get( 'prix-mini' ),  $q->get( 'prix-maxi' ) ),
		'compare' => 'BETWEEN',
		'type'    => 'NUMERIC',
	    );
} 
// si juste le prix mini est spécifié
elseif ( $q->get( 'prix-mini' ) ) {
	$meta_queries[] = array(
		'key'     => 'prix',
		'value'   => $q->get( 'prix-mini' ),
		'compare' => '>=',
		'type'    => 'NUMERIC',
	    );
} 
// si on a juste un prix maxi
elseif ( $q->get( 'prix-maxi' ) ) {
	$meta_queries[] = array(
		'key'     => 'prix',
		'value'   => $q->get( 'prix-maxi' ),
		'compare' => '<=',
		'type'    => 'NUMERIC',
	    );
}