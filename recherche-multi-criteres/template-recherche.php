<?php
add_action( 'template_include', 'willy_recherche_template' );
function willy_recherche_template( $template ) {
	// S'il s'agit d'une recherche
	if ( is_search()
	  // et plus précisemment d'une recherche multi-critères
	  && ( get_query_var( 'ville' ) 
		|| get_query_var( 'chambres' ) 
		|| get_query_var( 'quartiers' ) 
		|| get_query_var( 'prix-mini' )
		|| get_query_var( 'prix-maxi' )
		|| get_query_var( 'equipements' ) ) ) {
			// on essaye de charger notre template de résultats de recherche
			$new_template = locate_template( array( 'recherche-multicriteres.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}
	}
	return $template;
}