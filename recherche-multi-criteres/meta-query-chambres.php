<?php
// on récupère ou on créer un tableau meta_query
$meta_queries = $q->get( 'meta_query', array() );

// Si un nombre de chambres est spécifié
if ( $q->get( 'chambres' ) ) {
	$meta_queries[] = array(
		'key'     => 'chambres',
		'value'   => $q->get( 'chambres' ),
		'compare' => '>=',
		'type'    => 'NUMERIC',
	    );
}