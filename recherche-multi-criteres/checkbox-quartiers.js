jQuery(document).ready( function($) {
	if ( document.getElementById('ville') ) {
		$ville = $('#ville');
		$ville.on( 'change', function() {
			$.ajax({
				url: adminAjax,
				method: 'GET',
				data: {
					action: 'select_quartiers',
					ville: $ville.val(),
				},
				success: function( data ) {
					if ( data.success ) {
						$('#quartier-wrapper').html( data.data.checklist );
					}
				}
			})
		} );
	}
});