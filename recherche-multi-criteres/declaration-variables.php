<?php
add_filter( 'query_vars', 'willy_add_query_vars' );
function willy_add_query_vars( $vars ){
	$vars[] = "ville";
	$vars[] = "chambres";
	$vars[] = "quartiers";
	$vars[] = "prix-mini";
	$vars[] = "prix-maxi";
	$vars[] = "equipements";
	return $vars;
}