<?php
function get_user_coords() {
	// L'IP de notre utilisateur
	$ip = get_user_ip();
	if ( ! $ip ) {
		return false;
	}
		
	// … au format littéral
	$ip = ip2long( $ip );
	if ( ! $ip ) {
		return false;
	}

	global $wpdb;
	$infos = $wpdb->get_row( 
    			$wpdb->prepare( 
    				"SELECT * FROM {$wpdb->prefix}ip2location_db5 WHERE %d BETWEEN ip_from AND ip_to", 
    				$ip ) );
	if ( $infos ) {
		return array(
		'lat' => $infos->latitude;
		'lng' => $infos->longitude;
		);
		// Nous obtenons aussi :
		// La ville : $infos->city_name;
		// La région : $infos->region_name;
		// Le pays : $infos->country_name;
		// Le code Pays : $infos->country_code;
	}
}