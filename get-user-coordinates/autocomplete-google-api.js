function doAutoComplete() {
	// On cible notre input
	var input = document.getElementById('adresse');
	var options = {
	  types: ['(cities)'], // Ne proposer que les villes
	  componentRestrictions: {country: 'fr'} // uniqument les villes françaises, d'ailleurs
	};

	autocomplete = new google.maps.places.Autocomplete( input, options );
}

// Chargement asynchrone de la librairie Goole Maps places
window.onload = function() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&callback=doAutoComplete';
	document.body.appendChild(script);	
}