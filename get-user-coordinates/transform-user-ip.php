<?php
// Mon ip
$ip = '78.202.225.85';
return ip2long( $ip ); 
// ip2long() retourne l'adresse au format littéral

// … cela revient ± à faire :
$ip = explode( '.', $ip );
return $ip1[0] * pow( 256, 3 ) 
		+ $ip1[1] * pow( 256, 2 ) 
		+ $ip1[2] * 256 
		+ $ip1[3];