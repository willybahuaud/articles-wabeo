<?php
add_action( 'init', 'get_coords_from_form' );
function get_coords_from_form() {
	if ( ! is_admin()
	  && isset( $_POST['adresse'] ) ) {
	  	// On construit une URL pour envoyer une requête à l'API Google
	  	$url = 'https://maps.googleapis.com/maps/api/geocode/json';
	  	$adresse = sanitize_text_field( $_POST['adresse'] );
	  	$url = add_query_arg( array( 'address', $adresse ), $url );

	  	$requete = wp_remote_get( $url );
	  	if ( 200 == wp_remote_retrieve_response_code( $requete ) ) {
	  		$result = json_decode( wp_remote_retrieve_body( $requete ) );
	  		if ( isset( $results->results[0]->geometry->location->lat,
	  			  $results->results[0]->geometry->location->lng ) ) {
	  			$lat = $results->results[0]->geometry->location->lat;
	  			$lng = $results->results[0]->geometry->location->lng;
	  			// Do stuff
	  		}
	  	}
	}
}