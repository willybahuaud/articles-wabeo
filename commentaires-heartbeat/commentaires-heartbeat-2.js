jQuery(document).ready( function($) {

    wp.heartbeat.setInterval( 15 );

    var args = {
        'time' : heartbeatCommentsAtts[ 'time' ],
        'id' : heartbeatCommentsAtts[ 'id' ]
    };
     
    wp.heartbeat.enqueue(
        'commentaire',
        args,
        false
    );

    $(document).on( 'heartbeat-tick.commentaire', function( event, data ) {
        if ( data.hasOwnProperty( 'commentaire' ) ) {
            if( data['commentaire'].comments ) {
                if( $( '.comment-list' ).length == 0 ) {
                    $( '#comments' ).prepend( '<h2 class="comments-title">1 Commentaire</h2><ol class="comment-list"></ol>');
                }
                var $list = $( '.comment-list' );
                $.each( data['commentaire'].comments, function(i,val) {
                    $list.append( val.html );
                } );
                var count = $list.find('li').length;
                var text  = ( count > 1 ) ? count + ' Commentaires' : '1 Commentaire'; 
                $( '.comments-title' ).text( text );
            }
            args.time = Math.round( new Date().getTime() / 1000 );
            wp.heartbeat.enqueue(
                'commentaire',
                args,
                false
            );
        }
    });

} );
