jQuery(document).ready( function($) {

    wp.heartbeat.setInterval( 15 );

    var args = {
        'time' : heartbeatCommentsAtts[ 'time' ],
        'id' : heartbeatCommentsAtts[ 'id' ]
    };
     
    wp.heartbeat.enqueue(
        'commentaire',
        args,
        false
    );

    $(document).on( 'heartbeat-tick.commentaire', function( event, data ) {
        if ( data.hasOwnProperty( 'commentaire' ) ) {
            if( data['commentaire'].comments ) {
                var $list = $( '.comment-list' );
                $.each( data['commentaire'].comments, function(i,val) {
                    $list.append( val.html );
                } );
            }
            args.time = Math.round( new Date().getTime() / 1000 );
            wp.heartbeat.enqueue(
                'commentaire',
                args,
                false
            );
        }
    });

} );
