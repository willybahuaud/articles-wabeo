<?php
/**
Plugin Name: Plugin commentaires
Description: Les commentaires qui se chargent avec l'heartbeat
Author: willybahuaud
Version: 0.9
*/

add_action( 'wp_enqueue_scripts', 'heartbeat_enqueue_scripts' );
function heartbeat_enqueue_scripts(){
    if( is_single() ) {
        wp_register_script( 'heartbeat-comments', plugin_dir_url( __FILE__ ) . 'commentaires-heartbeat.js', 'heartbeat', '0.9', true );
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'heartbeat' );
        wp_enqueue_script( 'heartbeat-comments' );
    }
}


function commentaire_respond_to_server( $response, $data, $screen_id ) {
    if ( isset( $data['commentaire'] ) ) {
        $response['commentaire'] = array(
            'hello' => 'world'
        );
    }
    return $response;
}
 add_filter( 'heartbeat_received', 'commentaire_respond_to_server', 10, 3 );
 add_filter( 'heartbeat_nopriv_received', 'commentaire_respond_to_server', 10, 3 );

