<?php
/**
Plugin Name: Plugin commentaires
Description: Les commentaires qui se chargent avec l'heartbeat
Author: willybahuaud
Version: 0.9
*/

add_action( 'wp_enqueue_scripts', 'heartbeat_enqueue_scripts' );
function heartbeat_enqueue_scripts(){
    if( is_single() ) {
        global $post;
        wp_register_script( 'heartbeat-comments', plugin_dir_url( __FILE__ ) . 'commentaires-heartbeat.js', 'heartbeat', '0.9', true );
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'heartbeat' );
        wp_enqueue_script( 'heartbeat-comments' );
        wp_localize_script( 'heartbeat-comments', 'heartbeatCommentsAtts', array( 'id' => $post->ID, 'time' => time() ) );
    }
}

function commentaire_respond_to_server( $response, $data, $screen_id ) {
    if ( isset( $data['commentaire'] ) ) {
        if ( ! isset( $data['commentaire']['id'], $data['commentaire']['time'] ) )
            return false;
        $time     = $data['commentaire']['time'];
        $comments = get_comments( array(
            'post_id'      => $data['commentaire']['id'],
            'status'       => 'approve',
            'comment_type' => '',
            'date_query'   => array(
                array(
                    'after' => array(
                        'year'   => date( 'Y', $time ),
                        'month'  => date( 'm', $time ),
                        'day'    => date( 'd', $time ),
                        'hour'   => date( 'H', $time ),
                        'minute' => date( 'i', $time ),
                        'second' => date( 's', $time )
                        )
                    )
                )
            ) );
        if( $comments ) {
            $commentaires = array();
            foreach( $comments as $comment ) {
                $html = '<li id="' . esc_attr( $comment->comment_ID ) . '">';
                $html .= '<div class="comment-infos">' . wp_sprintf( 'Par %s', get_comment_author_link( $comment->comment_ID ) ) . '<span class="comment-time"> &mdash; Publié à l\'instant</span></div>';
                $html .= get_avatar( $comment->comment_author_email, 200, 'Mystery Man', $comment->comment_author );
                $html .= '<div class="comment-text">';
                $html .= apply_filters( 'comment_text', get_comment_text( $comment->comment_ID ), $comment );
                $html .= '</div>';
                $html .= '</li>';

                $commentaires[] = array( 'comment' => $comment, 'html' => $html );
            }
        }

        $response['commentaire'] = array(
            'comments' => $commentaires
        );
    }
    return $response;
}
 
add_filter( 'heartbeat_received', 'commentaire_respond_to_server', 10, 3 );
add_filter( 'heartbeat_nopriv_received', 'commentaire_respond_to_server', 10, 3 );

