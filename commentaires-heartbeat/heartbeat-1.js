jQuery(document).ready( function($) {

    var args = {
        'version': '3.6',
    };
     
    wp.heartbeat.enqueue(
        'commentaire',
        args,
        false
    );

    $(document).on( 'heartbeat-tick.commentaire', function( event, data ) {
        console.log( 'beat' );
        if ( data.hasOwnProperty( 'commentaire' ) ) {
            console.log( data[ 'commentaire' ] );            
            wp.heartbeat.enqueue(
                'commentaire',
                args,
                false
            );
        }
    });

} );
