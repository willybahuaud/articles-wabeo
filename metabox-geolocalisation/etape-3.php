<?php
/*
Étape 3
 */

// Ajout de scripts
add_action( 'admin_enqueue_scripts', 'willy_register_scripts' );
function willy_register_scripts() {
	wp_register_script( 'leaflet', 'http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js' );
	wp_register_script( 'geo-script', plugins_url( 'js/geo-script.js', __FILE__ ), array( 'leaflet', 'jquery' ) );
	wp_register_style( 'leaflet-css', 'http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css' );
}

// Modification de la metabox
function willy_geo_metabox( $post ) {
	$geo_lat  = get_post_meta( $post->ID, 'lat', true );
	$geo_lng  = get_post_meta( $post->ID, 'lng', true );

	// Coordonnées par défaut
	$map = array(
	        'lat' => $geo_lat ? floatval( $geo_lat ) : 47.09,
	        'lng' => $geo_lng ? floatval( $geo_lng ) : -1.39 );

	wp_enqueue_style( 'leaflet-css' );
	wp_enqueue_script( 'geo-script' );
	wp_localize_script( 'geo-script', 'currentCoords', $map );
	// La petite map leaflet ira ici
	echo '<div id="place-pointer" class="place-pointer" style="height:250px;width:100%;"></div>';

	$coords = $geo_lng && $geo_lat ? implode( ',', array( $geo_lat, $geo_lng ) ) : '';
	echo '<input type="text" id="geo-coords" name="geo-coords" value="' . esc_attr( $coords ) . '" style="width:100%;">';

	wp_nonce_field( 'geo-save_' . $post->ID, 'geo-nonce') ;
}