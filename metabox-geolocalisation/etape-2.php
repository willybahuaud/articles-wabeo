<?php
/*
Étape 2
 */

// Sauvegarde de la metabox
add_action( 'save_post', 'willy_save_metabox' );
function willy_save_metabox( $post ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['geo-coords'] ) 
	  && preg_match( '/([0-9\.\-]+,[0-9\.\-]+)/', $_POST['geo-coords'] ) ) {

		check_admin_referer( 'geo-save_' . $_POST['post_ID'], 'geo-nonce' );

		$coords = array_map( 'trim', explode( ',', $_POST['geo-coords'] ) );
		update_post_meta( $_POST['post_ID'], 'lat', floatval( $coords[0] ) );
		update_post_meta( $_POST['post_ID'], 'lng', floatval( $coords[1] ) );
	}
}