<?php
/*
Étape 1
 */

// Ajout de la metabox
add_action( 'add_meta_boxes', 'willy_load_metabox', 10, 2 );
function willy_load_metabox( $post_type, $post ) {
	if ( 'post' == $post_type ) {
		add_meta_box( 'willy-geo-coords', 'Coordonnées', 'willy_geo_metabox', $post_type, 'side', 'core' );
	}
}

// La metabox
function willy_geo_metabox( $post ) {
	$geo_lat  = get_post_meta( $post->ID, 'lat', true );
	$geo_lng  = get_post_meta( $post->ID, 'lng', true );

	$coords = $geo_lng && $geo_lat ? implode( ',', array( $geo_lat, $geo_lng ) ) : '';
	echo '<input type="text" id="geo-coords" name="geo-coords" value="' . esc_attr( $coords ) . '" style="width:100%;">';

	wp_nonce_field( 'geo-save_' . $post->ID, 'geo-nonce') ;
}