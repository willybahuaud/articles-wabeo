<?php
/*
 * Plugin Name: Geo metabox
 */

add_action( 'add_meta_boxes', 'willy_load_metabox', 10, 2 );
function willy_load_metabox( $post_type, $post ) {
	if ( 'post' == $post_type ) {
		add_meta_box( 'willy-geo-coords', 'Coordonnées', 'willy_geo_metabox', $post_type, 'side', 'core' );
	}
}

function willy_geo_metabox( $post ) {
	$geo_lat  = get_post_meta( $post->ID, 'lat', true );
	$geo_lng  = get_post_meta( $post->ID, 'lng', true );

	$map = array(
	        'lat' => $geo_lat ? floatval( $geo_lat ) : 47.09,
	        'lng' => $geo_lng ? floatval( $geo_lng ) : -1.39 );

	wp_enqueue_style( 'leaflet-css' );
	wp_enqueue_script( 'geo-script' );
	wp_localize_script( 'geo-script', 'currentCoords', $map );
	echo '<input type="search" id="geo-search" placeholder="Adresse..." style="width:100%;">';
	echo '<div id="place-pointer" class="place-pointer" style="height:250px;width:100%;"></div>';

	$coords = $geo_lng && $geo_lat ? implode( ',', array( $geo_lat, $geo_lng ) ) : '';
	echo '<input type="text" id="geo-coords" name="geo-coords" value="' . esc_attr( $coords ) . '" style="width:100%;">';

	wp_nonce_field( 'geo-save_' . $post->ID, 'geo-nonce') ;
}

add_action( 'save_post', 'willy_save_metabox' );
function willy_save_metabox( $post ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['geo-coords'] ) && trim( $_POST['geo-coords'] ) != '' ) {
		check_admin_referer('geo-save_'.$_POST['post_ID'], 'geo-nonce');

		$coords = array_map( 'trim', explode( ',', $_POST['geo-coords'] ) );
		update_post_meta( $_POST['post_ID'], 'lat', floatval( $coords[0] ) );
		update_post_meta( $_POST['post_ID'], 'lng', floatval( $coords[1] ) );
	}
}

add_action( 'admin_enqueue_scripts', 'willy_register_scripts' );
function willy_register_scripts() {
	wp_register_script( 'leaflet', 'http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js' );
	wp_register_script( 'geo-script', plugins_url( 'js/geo-script.js', __FILE__ ), array( 'leaflet', 'jquery' ) );
	wp_register_style( 'leaflet-css', 'http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css' );
}
