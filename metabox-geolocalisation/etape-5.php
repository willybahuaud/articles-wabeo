<?php
/*
Étape 5
 */

// Ajout d'un champ de recherche par adresse
function willy_geo_metabox( $post ) {
	$geo_lat  = get_post_meta( $post->ID, 'lat', true );
	$geo_lng  = get_post_meta( $post->ID, 'lng', true );

	$map = array(
	        'lat' => $geo_lat ? floatval( $geo_lat ) : 47.09,
	        'lng' => $geo_lng ? floatval( $geo_lng ) : -1.39 );

	wp_enqueue_style( 'leaflet-css' );
	wp_enqueue_script( 'geo-script' );
	wp_localize_script( 'geo-script', 'currentCoords', $map );
	echo '<input type="search" id="geo-search" placeholder="Adresse..." class="widefat">';
	echo '<div id="place-pointer" class="place-pointer" style="height:250px;width:100%;"></div>';

	$coords = $geo_lng && $geo_lat ? implode( ',', array( $geo_lat, $geo_lng ) ) : '';
	echo '<input type="text" id="geo-coords" name="geo-coords" value="' . esc_attr( $coords ) . '" style="width:100%;">';

	wp_nonce_field( 'geo-save_' . $post->ID, 'geo-nonce') ;
}