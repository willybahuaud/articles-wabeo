jQuery( document ).ready(function( $ ) {
	if( $( '#place-pointer' ).length > 0 ) {
		var map = L.map( 'place-pointer' ).setView( [ currentCoords['lat'], currentCoords['lng'] ], 13);
		L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo( map );
		var fmarker = L.marker([ currentCoords['lat'], currentCoords['lng'] ], {
			draggable:true
		} ).addTo( map );

		fmarker.on( 'dragend', function(event){
			var marker = event.target;
			var position = marker.getLatLng();
			map.setView( position, map.getZoom() );
			var coords = position.lat.toFixed(7) + ',' + position.lng.toFixed(7);
			$( '#geo-coords' ).val( coords );
		} );
	
		// lorsque l'on presse la touche entrée sur notre champ de recherche…
		// … on envoie une requête ajax à l'API geocode de google maps…
		// … puis on déplace le pointer et on met à jour notre input
		$( '#geo-search' ).on( 'keydown', function( e ) {
			if ( e.keyCode === 13 ) { // 13 = touche entrée
				e.preventDefault();
				$.ajax({
					method: 'GET',
					url: 'https://maps.googleapis.com/maps/api/geocode/json',
					data: {
						address: $( this ).val()
					},
					success: function (data) {
						if ( typeof data.results[0].geometry.location != 'undefined' ) {
							var coords = data.results[0].geometry.location;
							var newLatLng = new L.LatLng( coords.lat, coords.lng );
							fmarker.setLatLng( newLatLng );
							map.setView( coords, map.getZoom() );
							coords = coords.lat.toFixed(7) + ',' + coords.lng.toFixed(7);
							$( '#geo-coords' ).val( coords );
						}
					}
				});
			}
		});
	}
});