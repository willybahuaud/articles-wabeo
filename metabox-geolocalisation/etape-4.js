jQuery( document ).ready(function( $ ) {
	if( $( '#place-pointer' ).length > 0 ) {
		// Création de la carte avec les coordonnées existantes…
		// … ou celles par défaut
		var map = L.map( 'place-pointer' ).setView( [ currentCoords['lat'], currentCoords['lng'] ], 13);
		L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' ).addTo( map );
		var fmarker = L.marker([ currentCoords['lat'], currentCoords['lng'] ], {
			draggable:true
		} ).addTo( map );

		// Lorsque l'on déplace le pointeur…
		// … mettre à jour notre input
		fmarker.on( 'dragend', function(event){
			var marker = event.target;
			var position = marker.getLatLng();
			map.setView( position, map.getZoom() );
			var coords = position.lat.toFixed(7) + ',' + position.lng.toFixed(7);
			$( '#geo-coords' ).value = coords;
		} );
	}
});