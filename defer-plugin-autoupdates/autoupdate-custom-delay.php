<?php
/*
Plugin name: Defer plugin auto-updates
Description: Wait 3 days before autoupdate a plugin
 */

defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

add_filter( 'auto_update_plugin', 'willy_defer_plugin_autoupdates', 10, 2 );
function willy_defer_plugin_autoupdates( $update, $item ) {
	if ( ! $update_defer_array = get_site_option( 'deferred_plugin_autoupdates' ) ) {
		$update_defer_array = array();
	}

	$name = $item->slug;
	$version = $item->new_version;

	$delay = in_array( $name, array(
	            'wordpress-seo',
	            'woocommerce',
	            ) ) ? DAY_IN_SECONDS * 10 : DAY_IN_SECONDS * 3;

	if ( in_array( $name, array_keys( $update_defer_array ) )
	  && $version == $update_defer_array[ $name ]['version'] ) {
	  	if ( time() > $update_defer_array[ $name ]['age'] + $delay ) {

		  	// Unset this queue
		  	unset( $update_defer_array[ $name ] );
		  	update_site_option( 'deferred_plugin_autoupdates', $update_defer_array );
		  	
			return true;
		} else {
			return false;
		}
	}

	$update_defer_array[ $name ] = array( 'version' => $version, 'age' => time() );
	update_site_option( 'deferred_plugin_autoupdates', $update_defer_array );

	return false;
}