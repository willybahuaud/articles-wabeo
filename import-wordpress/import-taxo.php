<?php

// Taxonomies Table
// Ex : old_categories
//  ____________________________________
// |id| nom |  slug   |   description   |
// |--|-----|---------|-----------------|
// | 4|Cat 1|category1|Ceci est ma cat 1|
// | 7|Cat 2|category2|Ceci est ma cat 2|

// Tableau de taxonomies
global $wpdb;
$categories = $wpdb->get_results( "SELECT * FROM old_categories" );

// Import des termes
$taxo = 'category';
$old_category_correspondance = array();
foreach ( $categories as $category ) {
	if ( ! term_exists( $category->nom, $taxo ) ) {
		$term = wp_insert_term( $category->nom, $taxo, array(
		         'description' => $category->description,
		 		) );
		if ( ! is_wp_error( $term ) ) {
			update_term_meta( $term->term_id, 'ancien_id', $category->id );
			$old_category_correspondance[ $category->id ] = $term['term_id'];
		}
	}
}
// Nous servira ensuite pour les correspondance d'ID
update_option( 'old_categories_correspondance', $old_category_correspondance );


